package com.company;

import java.util.ArrayList;

public class ParkingManager {
	public static void main(String[] args) {
		try {
			while (true) {
				Parking parking = new Parking();
				UserInterface user = new UserInterface();
				System.out.println("Enter number of car places");
				int carPlaces = parking.setCarPlaces();
				System.out.println("Enter number of truck places");
				int truckPlaces = parking.setTruckPlaces();
				int allPlaces = carPlaces + truckPlaces;
				ArrayList<Car> parkingPlaces = parking.setParkingPlaces(allPlaces);
				ArrayList<Car> carsList = parking.createCars(parking.setAmountOfCars(allPlaces));
				ArrayList<Truck> trucksList = parking.createTrucks(parking.setAmountOfCars(allPlaces));
				parking.fillParkingPlace(parkingPlaces, trucksList, carsList, truckPlaces);
				while (true) {
					int command = user.checkCommand(parking, parkingPlaces, trucksList, carsList, truckPlaces);
					if (command == 8) {
						return;
					}
					if (command == 0) {
						trucksList = parking.createTrucks(parking.setAmountOfCars(allPlaces));
						carsList = parking.createCars(parking.setAmountOfCars(allPlaces));
						parking.fillParkingPlace(parkingPlaces, trucksList, carsList, truckPlaces);
					}
				}

			}
		} catch (Exception e) {
			System.out.println("NAN");
		}
		
	}
}
