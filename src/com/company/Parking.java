package com.company;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Parking {
	Scanner scanner = new Scanner(System.in);

	int setCarPlaces() {
		while (true) {
			int carPlaces = scanner.nextInt();
			if (carPlaces >= 3) {
				return carPlaces;
			} else {
				System.out.println("Pls, enter the value that more than 3, otherwise the program doesn't make any sense(because cars arriving less than 1/3 of places)");
			}
		}
	}

	int setTruckPlaces() {
		while (true) {
			int truckPlaces = scanner.nextInt();
			if (truckPlaces > 1) {
				return truckPlaces;
			} else {
				System.out.println("Pls, enter the value that more than 1, otherwise the program doesn't make any sense(because cars arriving less than 1/3 of places)");
			}
		}
	}

	ArrayList<Car> setParkingPlaces(int allPlaces) {
		ArrayList<Car> parkingPlaces = new ArrayList<>(allPlaces);
		for (int i = 0; i < allPlaces; i++) {
			parkingPlaces.add(i,new Car());
		}
		return parkingPlaces;
	}

	int setAmountOfCars(int places) {
		Random random = new Random();
		int amountOfCars = 0;
		int minimum = 1;
		int maximum = places / 3;
		while (amountOfCars == 0) {
			amountOfCars = random.nextInt(maximum - minimum);
		}
		amountOfCars += minimum;
		return amountOfCars;
	}

	ArrayList<Car> createCars(int numberOfCars) {
		ArrayList<Car> carsList = new ArrayList<>(numberOfCars);
		for (int i = 0; i < numberOfCars; i++) {
			carsList.add(i,new Car(setTurns(),setNumberOfCar()));
		}
		return carsList;
	}

		ArrayList<Truck> createTrucks(int numberOfTruck) {
		ArrayList<Truck> trucksList = new ArrayList<>(numberOfTruck);
		for (int i = 0; i < numberOfTruck; i++) {
			trucksList.add(i,new Truck(setTurns(),setNumberOfCar()));
		}
		return trucksList;
	}

	String setNumberOfCar() {
		Random random = new Random();
		int first = random.nextInt(10);
		int second = random.nextInt(10);
		int third = random.nextInt(10);
		int fourth = random.nextInt(10);
		String number = String.valueOf(first) + second + third + fourth;
		return number;
	}

	int setTurns() {
		int minimum = 1;
		int maximum = 10;
		int turns;
		int difference = maximum - minimum;
		Random random = new Random();
		turns = random.nextInt(difference);
		turns += minimum;
		return turns;
	}


	ArrayList<Car> explodeCars(ArrayList<Car> places) {
		for (int i = 0; i < places.size(); i++) {
			places.get(i).turns = 0;
			places.get(i).number = "";
		}
		return places;
	}


	void showNearestAvailabilityOfPlace(ArrayList<Car> cars) {
		int minimumTurn = 11;
		for (int i = 0; i < cars.size(); i++) {
			if (cars.get(i).turns < minimumTurn && cars.get(i).turns > 0) {
				minimumTurn = cars.get(i).turns;
			}
		}
		for (int i = 0; i < cars.size(); i++) {
			if (cars.get(i).turns == minimumTurn) {
				System.out.println("The place "+i+" will be free in "+cars.get(i).turns+" turns");
			}
		}
	}

	int checkPlaces(ArrayList<Car> places, int command) {
		int freePlaces = 0;
		int takenPlaces = 0;
		for (int i = 0; i < places.size(); i++) {
			if (places.get(i).turns == 0) {
				freePlaces++;
			} else {
				takenPlaces++;
			}
		}
		if (command == 1) {
			return freePlaces;
		}
		return takenPlaces;
	}

	void trucksAtThisTurn(ArrayList<Truck> trucksList) {
		if (trucksList.size() == 0) {
			System.out.println("Nobody came on the meeting of subscribers... ");
		}
		for (int i = 0; i < trucksList.size(); i++) {
			System.out.println("The truck " + trucksList.get(i).number + " came to parking and have " + trucksList.get(i).turns + " turns");
		}
	}
	void carsAtThisTurn(ArrayList<Car> carsList) {
		if (carsList.size() == 0) {
			System.out.println("Nobody came on the meeting of subscribers... ");
		}
		for (int i = 0; i < carsList.size(); i++) {
			System.out.println("The car " + carsList.get(i).number + " came to parking and have " + carsList.get(i).turns + " turns");
		}
	}

	ArrayList<Car> fillParkingPlace(ArrayList<Car> parkingPlaces, ArrayList<Truck> trucksList, ArrayList<Car> carsList, int truckPlaces) {
		int leftCars = 0;
		int leftTrucks = 0;

		for (int i = 0; i < truckPlaces; i++) {
			if (parkingPlaces.get(i).turns != 0) {
				parkingPlaces.get(i).turns -= 1;
			}
		}
		int truckPlaceCounter = 0;
		for (int i = 0; i < trucksList.size(); i++) {
			int trucksCounter = -1;
			for (int j = 0; j < truckPlaces; j++) {
				if (parkingPlaces.get(j).turns == 0) {
					parkingPlaces.get(j).turns = trucksList.get(truckPlaceCounter).turns;
					parkingPlaces.get(j).number = trucksList.get(truckPlaceCounter).number;
					trucksCounter = 0;
					break;
				}
			}
			truckPlaceCounter++;
			if (trucksCounter != 0) {
				leftTrucks++;
			}
		}
		int placeCounter = 0;
		for (int i = 0; i < carsList.size(); i++) {
			int carsCounter = -1;
			for (int j = truckPlaces; j < parkingPlaces.size(); j++) {
				if (parkingPlaces.get(j).turns == 0) {
					parkingPlaces.get(j).turns = carsList.get(placeCounter).turns;
					parkingPlaces.get(j).number = carsList.get(placeCounter).number;
					carsCounter = 0;
					break;
				}
			}
			placeCounter++;
			if (carsCounter != 0) {
				leftCars++;
			}
		}

		int leftTrucksFromPlaces = 0;
		for (int i = trucksList.size() - leftTrucks; i < trucksList.size(); i++) {
			int trucksCounter = -1;
			for (int j = truckPlaces; j < parkingPlaces.size() - 1; j++) {
				if (parkingPlaces.get(j).turns == 0 && parkingPlaces.get(j+1).turns == 0) {
					parkingPlaces.get(j).turns = trucksList.get(i).turns;
					parkingPlaces.get(j+1).turns = trucksList.get(i).turns;
					parkingPlaces.get(j).number = trucksList.get(i).number;
					parkingPlaces.get(j+1).number = trucksList.get(i).number;
					trucksCounter = 0;
					break;
				}
			}
			if (trucksCounter != 0) {
				leftTrucksFromPlaces++;
			}
		}

		leftTrucks = leftTrucksFromPlaces;

		if (leftCars != 0 && leftTrucks != 0) {
			System.out.println();
			System.out.println("The " + leftCars + " of cars drive away and the " + leftTrucks + " of trucks drive away because we don't have places for them");

		} else if (leftTrucks != 0) {
			System.out.println();
			System.out.println("the " + leftTrucks + " of trucks drive away because we don't have free places for them");
		} else if (leftCars != 0) {
			System.out.println();
			System.out.println("the " + leftCars + " of cars drive away because we don't have free places for them");
		}

		return parkingPlaces;
	}

	void getInfoAboutParking(ArrayList<Car> parkingPlaces, int parkingPlaceTrucks) {
		for (int i = 0; i < parkingPlaceTrucks; i++) {
			if (parkingPlaces.get(i).turns == 0) {
				System.out.println("Truck place "+(i + 1) + " don't have truck");
				continue;
			}
			System.out.println("On truck place " + (i + 1) + " stay truck " + parkingPlaces.get(i).number + " with " +
					parkingPlaces.get(i).turns + " turns");
		}

		for (int i = parkingPlaceTrucks; i < parkingPlaces.size(); i++) {

			if (parkingPlaces.get(i).turns == 0) {
				System.out.println("Car place " + (i + 1) + " don't have car ");
				continue;
			}
			if ((i != parkingPlaces.size() - 1) && (parkingPlaces.get(i).number.equals(parkingPlaces.get(i+1).number))) {
				System.out.println("On car place " + (i + 1) + " and place " + (i + 2) + " stay truck " + parkingPlaces.get(i).number
						+ " with "
						+ parkingPlaces.get(i).turns + " turns");
				i++;
				continue;

			}
			System.out.println("On car place " + (i + 1) + " stay car " + parkingPlaces.get(i).number + " with "
					+ parkingPlaces.get(i).turns+" turns");
		}
		System.out.println();
	}

	public Parking() {
	}
}