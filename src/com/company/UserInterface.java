package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class UserInterface {
	Scanner scanner = new Scanner(System.in);
	void help() {
		System.out.println("You can use this commands:");
		System.out.println("Make a turn(enter 0)");
		System.out.println("How much places is free?(enter 1)");
		System.out.println("How much places is taken?(enter 2)");
		System.out.println("What place will be free soon?(enter 3)");
		System.out.println("Explode all cars(and make free parking places)(enter 4)");
		System.out.println("What trucks arrived at this turn?(enter 5)");
		System.out.println("What cars arrived at this turn?(enter 6)");
		System.out.println("Show info about parking and it's places(enter 7)");
		System.out.println("Exit the program(enter 8)");
	}

	int checkCommand(Parking parking1, ArrayList<Car> cars, ArrayList<Truck> trucks, ArrayList<Car> carsList, int truckPlaces) {
		while (true) {
			help();
			while (true) {
				System.out.println("Enter the command:");
				int command = scanner.nextInt();
				switch (command) {
					case 0:
						return command;
					case 1:
						int freePlaces = parking1.checkPlaces(cars,1);
						System.out.println("We have "+freePlaces+" free places");
						break;
					case 2:
						int takenPlaces = parking1.checkPlaces(cars,2);
						System.out.println("We have "+takenPlaces+" taken places");
						break;
					case 3:
						parking1.showNearestAvailabilityOfPlace(cars);
						break;
					case 4:
						cars = parking1.explodeCars(cars);
						System.out.println("All places are free now, but at what cost.... ");
						break;
					case 5:
						parking1.trucksAtThisTurn(trucks);
						break;
					case 6:
						parking1.carsAtThisTurn(carsList);
						break;
					case 7:
						parking1.getInfoAboutParking(cars,truckPlaces);
						break;
					case 8:
						System.out.println("Ok bb");
						return command;
					default:
						System.out.println("It's not what it seems... Try again");
				}
			}
		}
	}
}
